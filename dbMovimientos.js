const { ObjectId } = require('mongodb');

// db1.js
var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";

module.exports = {
    GetMovements: async function(userId) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('movimientos');
            let result = await dCollection.find({id: userId});
            return result.toArray();
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    GetMovementDetail: async function(id) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('movimientos');
            let result = await dCollection.findOne({_id: ObjectId(id)});
            return result;
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    PostMovement: async function(movement) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('movimientos');
            let result = await dCollection.insert(movement);
            return result;
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    }
}
var express = require('express'),
	app = express(),
	port = process.env.PORT || 3001;

app.listen(port);

// Dependencies
var localhost = "localhost:8080";
var bodyparser = require('body-parser');
var auth = require('./auth');
const dbMovimientos = require('./dbMovimientos');
const dbCuentas = require('./dbCuentas');
var jwt = require('jsonwebtoken');

app.use(bodyparser.json())
app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
	res.header("Access-Control-Allow-Origin", "*")
	res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization")
	next()
})

console.log('todo list RESTful API server started on: ' + port);

// LOGIN
app.post("/login", function (req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var email = req.body.email
	var password = req.body.password
	auth.FindUser({"email": email, "password": password}).then(function (user) {
		delete user["password"];
		var token = jwt.sign(user, auth.getSecret(), {expiresIn: 60 * 60 * 24});
		res.end(JSON.stringify({ user: user, token: token }));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});

app.post("/verify-token", auth.verifyToken(), function (req, res) {
	res.end(JSON.stringify({success: true}));
});


// Movimientos
app.get("/movimientos", auth.verifyToken(), function(req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var id = auth.getUserId(req.headers["authorization"]);
	dbMovimientos.GetMovements(id).then(function (movimientos) {
		res.end(JSON.stringify(movimientos));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});

app.get("/movimientos/:id", auth.verifyToken(), function(req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var movementId = req.params.id;
	dbMovimientos.GetMovementDetail(id).then(function (movimiento) {
		console.log(movimiento);
		res.end(JSON.stringify(movimiento));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});

app.post("/movimientos", auth.verifyToken(), function(req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var movement = req.body;
	dbCuentas.transfer(movement).then(function (movement) {
		res.end(JSON.stringify(movement));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});

app.get("/cuentas", auth.verifyToken(), function(req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var userId = auth.getUserId(req.headers["authorization"]);
	dbCuentas.GetAccounts(userId).then(function (account) {
		res.end(JSON.stringify(account));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});

app.get("/cuentas/:id", auth.verifyToken(), function(req, res) {
	res.set("Access-Control-Allow-Headers", "Content-Type");
	var accountId = req.params.id;
	console.log(req.params.id)
	dbCuentas.GetAccountDetail(accountId).then(function (cuenta) {
		console.log(cuenta);
		res.end(JSON.stringify(cuenta));
	}, function (err) {
		console.error('The promise was rejected', err, err.stack);
	});
});
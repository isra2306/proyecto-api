const { ObjectId } = require('mongodb');

// db1.js
var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";
var dbName = "cuentas";

const transfer = async function(transferData) {
    let client, db;
    try {
        client = await MongoClient.connect(mongoUrl, {
            useNewUrlParser: true
        });
        // Obtener cuenta origen y destino
        var cuentaOrigen = await getAccount(transferData.cuentaOrigen);
        var cuentaDestino = await getAccount(transferData.cuentaDestino);
        // Revisar que sea posible hacer la transferencia
        if(cuentaOrigen.saldo - transferData.importe < 0) {
            console.log("Saldo insuficiente!");
            return undefined;
        }
        // Deducir saldo de cuenta origen e insertar movimiento
        cuentaOrigen.saldo -= transferData.importe;
        transferData["tipo"] = 1;
        cuentaOrigen.movimientos.push(transferData);
        console.log(cuentaOrigen.movimientos);
        // Abonar saldo en cuenta destino e insertar movimiento
        cuentaDestino.saldo += Number(transferData.importe);
        transferData["tipo"] = 2;
        cuentaDestino.movimientos.push(transferData);
        console.log(cuentaDestino.movimientos);

        db = client.db("practitioner");
        let dCollection = db.collection('cuentas');
        await dCollection.updateOne(
            {"numero": cuentaOrigen.numero},
            {$set: {"saldo": cuentaOrigen.saldo, "movimientos": cuentaOrigen.movimientos}},
            { upsert: true }
        );

        let result = await dCollection.updateOne(
            {"numero": cuentaDestino.numero},
            {$set: {"saldo": cuentaDestino.saldo, "movimientos": cuentaDestino.movimientos}},
            { upsert: true }
        );
        return result;
    } catch (err) {
        console.error(err);
    } // catch any mongo error here
    finally {
        client.close();
    } // make sure to close your connection after
}

const getAccount = async function(accountNumber) {
    let client, db;
    try {
        client = await MongoClient.connect(mongoUrl, {
            useNewUrlParser: true
        });
        db = client.db("practitioner");
        let dCollection = db.collection('cuentas');
        let result = await dCollection.findOne({"numero": accountNumber});
        return result;
    } catch (err) {
        console.error(err);
    } // catch any mongo error here
    finally {
        client.close();
    } // make sure to close your connection after
}

module.exports = {
    GetAccounts: async function(userId) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection(dbName);
            let result = await dCollection.find({userId: ObjectId(userId)});
            return result.toArray();
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    GetAccountDetail: async function(id) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection(dbName);
            let result = await dCollection.findOne({_id: ObjectId(id)});
            return result;
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    transfer: transfer,
}
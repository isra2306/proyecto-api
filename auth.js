// db1.js
var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";
var secret = Buffer.from('fe1a1915a379f3be5394b64d14794932', 'hex');
var jwt = require('jsonwebtoken');
var express = require('express')
const authGuard = express.Router(); 

exports.secret = secret;

module.exports = {
    getSecret: () => {
        return secret;
    },
    FindUser: async function (data) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('users');
            let result = await dCollection.findOne(data);
            return result;
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    verifyToken: () => {
        return authGuard.use((req, res, next) => {
            var token = req.headers["authorization"];
            if(token == null || token == "") {
                return res.json({success: false, msg: "No estás logeado"});
            }
            token = token.replace('Bearer ', '');
            jwt.verify(token, secret, function (err, user) {
                if (err) {
                    return res.json({success: false, msg: "No estás logeado"});
                } else {
                    next();
                }
            });
        });
    },
    getUserId(token) {
        return jwt.decode(token, secret)["_id"];
    }
}